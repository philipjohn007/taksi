'use strict';

var app = angular.module('taksiHomeApp');

app.service('PusherService', function ($rootScope) {
  var pusher = new Pusher('481bc50052c358efccb6');
  var channel1 = pusher.subscribe('drivers1');
  var channel2 = pusher.subscribe('drivers2');
  var channel3 = pusher.subscribe('drivers3');
  var channel4 = pusher.subscribe('drivers4');
  var channel5 = pusher.subscribe('drivers5');
  return {
    onMessage: function (callback) {
      channel1.bind('async_notification1', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
      channel2.bind('async_notification2', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
      channel3.bind('async_notification3', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
      channel4.bind('async_notification4', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
      channel5.bind('async_notification5', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});
