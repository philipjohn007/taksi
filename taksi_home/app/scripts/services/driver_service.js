'use strict';

var app = angular.module('taksiHomeApp');

app.service('DriverService', function ($resource) {
  console.log($resource);
  return $resource('http://taksi-server-pek.herokuapp.com/drivers', {});
});
