'use strict';

var app = angular.module('taksiHomeApp');

app.service('ConfirmationService', function ($resource) {
    return $resource('http://taksi-server-pek.herokuapp.com/drivers/accept', {});
});
