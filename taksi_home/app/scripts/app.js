'use strict';

/**
 * @ngdoc overview
 * @name taksiHomeApp
 * @description
 * # taksiHomeApp
 *
 * Main module of the application.
 */
angular
  .module('taksiHomeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/drivers/:id', {
        templateUrl: 'views/driver/new.html',
        controller: 'DriverController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
