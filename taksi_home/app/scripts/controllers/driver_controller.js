'use strict'

var app = angular.module('taksiHomeApp');

app.controller('DriverController', function ($scope, $routeParams, $http, $timeout, DriverService, PusherService, ConfirmationService) {
  $scope.status = '';
  $scope.syncNotification = '';
  $scope.id = $routeParams.id;
  $scope.accepted = false;
  $scope.driver = null;
  $scope.counter = 300;
  $scope.TaxiCounter = 3000;
  $scope.ms = '';
  $scope.invoice = '';

  var mytimeout2 = null;

  $http.get('http://taksi-server-pek.herokuapp.com/drivers/'+$scope.id).success(function(data, status, headers, config) {
    $scope.driver = data;
    $scope.status = $scope.driver.status;
    $scope.invoice = $scope.driver.totalamt*0.03;
  });

  $scope.submit = function() {
    DriverService.save({status: $scope.status, id: $scope.id}, function (response) {
      $scope.syncNotification = response.message;
    });
    if($scope.status != 'invisible') {
      $scope.counter = 300;
    }
  };

  $scope.asyncNotification = '';
  PusherService.onMessage(function(response) {
    $scope.TaxiCounter = 30;
    if($scope.id == response.id) {
      $scope.msgdiv = true;
      $scope.asyncNotification = response.message;
      mytimeout2 = $timeout($scope.onTimeout2,1000);
    }

   });

  $scope.accept = function() {
    ConfirmationService.save({id: $scope.id, asyncNotification: $scope.asyncNotification}, function (response) {
      $scope.accepted = true;
      $scope.syncNotification = response.message;
      $scope.status = 'busy';
      $scope.counter = 300;
      $scope.TaxiCounter = 3000;
      mytimeout2 = null;
    });
  };

  $scope.reject = function() {
    $scope.msgdiv = false;
    ConfirmationService.delete({id: $scope.id, asyncNotification: $scope.asyncNotification}, function (response) {
      $scope.syncNotification = response.message;
      $scope.counter = 300;
      $scope.TaxiCounter = 3000;
      mytimeout2 = null;
    });
  };

  $scope.onTimeout = function(){
    $scope.counter--;
    if ($scope.counter > 0) {
      mytimeout = $timeout($scope.onTimeout,1000);
      $scope.ms = '(' + Math.ceil($scope.counter / 60) + ' minutes remaining)'
    }
    else{
      $scope.counter = 0;
      $scope.status = 'invisible'
      $scope.ms = '';
      $scope.submit();
    }
  };

  $scope.onTimeout2 = function(){
    $scope.TaxiCounter--;
    mytimeout2 = $timeout($scope.onTimeout2,1000);
    if ($scope.TaxiCounter ==0 && $scope.msgdiv == true) {
      $scope.TaxiCounter =0;
      $scope.reject();
    }
  };

  var mytimeout = $timeout($scope.onTimeout,1000);


  $scope.reset= function(){
    $scope.counter = 5;
    mytimeout = $timeout($scope.onTimeout,1000);
  };

});
