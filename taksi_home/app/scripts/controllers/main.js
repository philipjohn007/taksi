'use strict';

/**
 * @ngdoc function
 * @name taksiHomeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the taksiHomeApp
 */
angular.module('taksiHomeApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
