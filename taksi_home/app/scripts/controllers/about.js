'use strict';

/**
 * @ngdoc function
 * @name taksiHomeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the taksiHomeApp
 */
angular.module('taksiHomeApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
