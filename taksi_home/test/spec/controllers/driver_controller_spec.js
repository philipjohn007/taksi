'use strict';

describe('DriverController', function () {

  beforeEach(module('taksiHomeApp'));

  var DriverController,
    scope,
    $httpBackend;

  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_, _DriverService_, _PusherService_) {
    scope = $rootScope.$new();
    $httpBackend = _$httpBackend_;

    DriverController = $controller('DriverController', {
      $scope: scope,
      $routeParams: {id: 1},
      DriverService: _DriverService_,
      PusherService: _PusherService_
    });
  }));

  it('should load the driver status', function () {
    $httpBackend.expectGET('http://taksi-server-pek.herokuapp.com/drivers/1').respond(200,'{"duration":"311","id":1,"location":"Kaubamaja, Tartu, Estonia","name":"Driver3","number":"1333","status":"available","totalamt":"0"}');
    $httpBackend.flush();

    expect(scope.id).toBe(1);
  });

  it('should update the driver status in the backend', function () {
    $httpBackend.expectGET('http://taksi-server-pek.herokuapp.com/drivers/1').respond(200,'{"duration":"311","id":1,"location":"Kaubamaja, Tartu, Estonia","name":"Driver3","number":"1333","status":"available","totalamt":"0"}');
    $httpBackend.flush();

    $httpBackend
      .expectPOST('http://taksi-server-pek.herokuapp.com/drivers',
      {status: 'available', id: 1})
      .respond(201, {message: 'Status updated'});

    scope.status = 'available';
    scope.id = 1;
    scope.submit();
    $httpBackend.flush();

    expect(scope.syncNotification).toBe('Status updated');
  });

  it('should accept the booking request', function () {
    $httpBackend.expectGET('http://taksi-server-pek.herokuapp.com/drivers/1').respond(200,'{"duration":"311","id":1,"location":"Kaubamaja, Tartu, Estonia","name":"Driver3","number":"1333","status":"available","totalamt":"0"}');
    $httpBackend.flush();

    $httpBackend
      .expectPOST('http://taksi-server-pek.herokuapp.com/drivers/accept',
      {id: 1, asyncNotification: 'Booking request arrived. ID: 102   Name:    Location: Raatuse 22, Tartu, Estonia   Destination: Liivi 2, Tartu, Estonia   Email:    Phone: '})
      .respond(200, {message: 'Customer Assigned'});

    scope.id = 1;
    scope.asyncNotification = 'Booking request arrived. ID: 102   Name:    Location: Raatuse 22, Tartu, Estonia   Destination: Liivi 2, Tartu, Estonia   Email:    Phone: ';
    scope.accept();
    $httpBackend.flush();

    expect(scope.syncNotification).toBe('Customer Assigned');
  });

  it('should reject the booking request', function () {
    $httpBackend.expectGET('http://taksi-server-pek.herokuapp.com/drivers/1').respond(200,'{"duration":"311","id":1,"location":"Kaubamaja, Tartu, Estonia","name":"Driver3","number":"1333","status":"available","totalamt":"0"}');
    $httpBackend.flush();

    $httpBackend
      .expectDELETE('http://taksi-server-pek.herokuapp.com/drivers/accept?asyncNotification=Booking+request+arrived.+ID:+102+++Name:++++Location:+Raatuse+22,+Tartu,+Estonia+++Destination:+Liivi+2,+Tartu,+Estonia+++Email:++++Phone:+&id=1').respond(200, {message: 'Booking Rejected'});

    scope.id = 1;
    scope.asyncNotification = 'Booking request arrived. ID: 102   Name:    Location: Raatuse 22, Tartu, Estonia   Destination: Liivi 2, Tartu, Estonia   Email:    Phone: ';
    scope.reject();
    $httpBackend.flush();

    expect(scope.syncNotification).toBe('Booking Rejected');
  });
});
