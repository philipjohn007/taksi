angular
    .module('taksi')
    .controller('BookingsController', function($scope, supersonic, $http, $rootScope) {
        $scope.name = '';
        $scope.address = '';
        $scope.email = '';
        $scope.phone = '';
        $scope.destination='';
        $scope.syncNotification = '';
        $scope.lat='';
        $scope.long='';
        $scope.asyncNotification='';

        supersonic.device.geolocation.getPosition().then( function(position) {
            $scope.lat=position.coords.latitude;
            $scope.long=position.coords.longitude;

            $http.get("https://taksi-server-pek.herokuapp.com//bookings/address?latitude="+$scope.lat+"&longitude="+$scope.long+"", {
            }).success(function (data, status, headers, config) {
                $scope.address = data.message;
                supersonic.logger.log("Response: " + data.message);
            }).error(function(data, status, headers, config){
                supersonic.logger.log("Error: "+status);
            });
        });
        var pusher = new Pusher('481bc50052c358efccb6');
        var channel = pusher.subscribe('bookings');

        channel.bind('async_notification', function (data) {
            $rootScope.$apply(function () {
                $scope.asyncNotification = data.message;
                supersonic.logger.log("Response: " + data.messsage);

            });
        });
        $scope.submit=function() {
            //supersonic.ui.dialog.alert("Email: " + $scope.address);
            supersonic.logger.log("Submitting");
            $http.post("http://taksi-server-pek.herokuapp.com/bookings", {
                name: $scope.name,
                address: $scope.address,
                destination: $scope.destination,
                email: $scope.email,
                phone: $scope.phone
            }).success(function (data, status, headers, config) {
                //$scope.syncNotification = response.message;
                $scope.syncNotification = data.message;
                supersonic.logger.log("Response: " + data.message);
            });
        };

    });
