'use strict'

angular
    .module('taksi')
    .controller('DriverController', function($scope, supersonic, $timeout,$http,$rootScope) {

        $scope.TaxiCounter = 3000;
        $scope.counter = 300;
        $scope.ms = '';
        $scope.driver = '';
        $scope.status = '';
        $scope.id = 2;
        var mytimeout2 = null;
        $scope.msgdiv = false;
        $scope.accepted = false;
        $scope.lat= '';
        $scope.long='';
		  $scope.invoice = '';
        $scope.invoicediv = false;
        $http.get('http://taksi-server-pek.herokuapp.com/drivers/2').success(function(data, status, headers, config) {

            $scope.driver = data;
            $scope.status = $scope.driver.status;
			$scope.invoice = $scope.driver.totalamt*0.03;
        });


        var pusher = new Pusher('481bc50052c358efccb6');
        var channel1 = pusher.subscribe('drivers1');
        var channel2 = pusher.subscribe('drivers2');
        var channel3 = pusher.subscribe('drivers3');
        var channel4 = pusher.subscribe('drivers4');
        var channel5 = pusher.subscribe('drivers5');
        $scope.asyncNotification = '';
        channel1.bind('async_notification1', function (data) {
            $rootScope.$apply(function () {
                callback(data);
            });
        });
        channel2.bind('async_notification2', function (data) {

            $rootScope.$apply(function () {

                $scope.TaxiCounter = 30;
                if($scope.id == data.id) {

                    $scope.asyncNotification = data.message;
                    $scope.accepted = false;
                    $scope.msgdiv = true;
                    mytimeout2 = $timeout($scope.onTimeout2,1000);
                }

            });
        });
        channel3.bind('async_notification3', function (data) {
            $rootScope.$apply(function () {
                callback(data);
            });
        });
        channel4.bind('async_notification4', function (data) {
            $rootScope.$apply(function () {
                callback(data);
            });
        });
        channel5.bind('async_notification5', function (data) {
            $rootScope.$apply(function () {
                callback(data);
            });
        });


        supersonic.device.geolocation.getPosition().then( function(position) {
            $scope.lat=position.coords.latitude;
            $scope.long=position.coords.longitude;
        });
		
		  $scope.showdlg = function() {
            alert('Your Invoice:' + $scope.invoice);
            $scope.invoicediv = true;

        };

        $scope.submit = function() {
               $http.post('http://taksi-server-pek.herokuapp.com/drivers?id='+$scope.id+'&status='+$scope.status+'&latitude='+$scope.lat+'&longitude='+$scope.long, {}).
                success(function(data, status, headers, config) {

                       $scope.syncNotification = data.message;
                       if($scope.status != 'invisible') {
                           $scope.counter = 300;
                       }
                }).
                error(function(data, status, headers, config) {

                });
        };



        $scope.accept = function() {
            $http.post('http://taksi-server-pek.herokuapp.com/drivers/accept', {id: $scope.id, asyncNotification: $scope.asyncNotification}).
                success(function(data, status, headers, config) {
                    $scope.accepted = true;
                    $scope.syncNotification = data.message;
                    $scope.status = 'busy';
                    $scope.counter = 300;
                    $scope.TaxiCounter = 3000;
                    mytimeout2 = null;
                }).
                error(function(data, status, headers, config) {

                });
        }



        $scope.reject = function() {
            $scope.msgdiv = false;
            $http.delete('http://taksi-server-pek.herokuapp.com/drivers/accept?id='+ $scope.id + '&asyncNotification=' + $scope.asyncNotification).
                success(function(data, status, headers, config) {

                        $scope.syncNotification = data.message;
                        $scope.counter = 300;
                        $scope.TaxiCounter = 3000;

                        mytimeout2 = null;
            });
        };




        $scope.onTimeout = function(){
            $scope.counter--;
            if ($scope.counter > 0) {
                mytimeout = $timeout($scope.onTimeout,1000);
                $scope.ms = '(' + Math.ceil($scope.counter / 60) + ' minutes remaining)'
            }
            else{
                $scope.counter = 0;
                $scope.status = 'invisible'
                $scope.ms = '';
                $scope.submit();
            }
        };

        $scope.onTimeout2 = function(){
            $scope.TaxiCounter--;
            mytimeout2 = $timeout($scope.onTimeout2,1000);
            if ($scope.TaxiCounter ==0 && $scope.msgdiv == true) {
                $scope.TaxiCounter =0;
                $scope.reject();
            }

            if ($scope.TaxiCounter < 0)
            {

                $scope.msgdiv == false;
                $scope.reject();
            }
        };


        var mytimeout = $timeout($scope.onTimeout,1000);


        $scope.reset= function(){
            $scope.counter = 5;
            mytimeout = $timeout($scope.onTimeout,1000);
        };

        $scope.navbarTitle = "Driver";
    });