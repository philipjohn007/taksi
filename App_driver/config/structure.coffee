# Read more about app structure at http://docs.appgyver.com

module.exports =

  # See styling options for tabs and other native components in app/common/native-styles/ios.css or app/common/native-styles/android.css
  tabs: [
    {
      title: "Home"
      id: "index"
      location: "taksi#getting-started" # Supersonic module#view type navigation
    }
    {
      title: "Orders"
      id: "Orders"
      location: "taksi#booking"
    }
    {
      title: "Map"
      id: "internet"
      location: "https://www.google.ee/maps/@58.3625265,26.7367371,13z?hl=en" # URLs are supported!
    }
  ]

  # rootView:
  #   location: "example#getting-started"

  preloads: [
    {
      id: "learn-more"
      location: "taksi#learn-more"
    }
    {
      id: "using-the-scanner"
      location: "taksi#using-the-scanner"
    }
  ]

  # drawers:
  #   left:
  #     id: "leftDrawer"
  #     location: "example#drawer"
  #     showOnAppLoad: false
  #   options:
  #     animation: "swingingDoor"
  #
  # initialView:
  #   id: "initialView"
  #   location: "example#initial-view"
