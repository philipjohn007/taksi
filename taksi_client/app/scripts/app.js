'use strict';

/**
 * @ngdoc overview
 * @name taksiClientApp
 * @description
 * # taksiClientApp
 *
 * Main module of the application.
 */
angular
  .module('taksiClientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/bookings', {
        templateUrl: 'views/bookings/new.html',
        controller: 'BookingsController'
      })
      .when('/driverhome', {
        templateUrl: 'views/driver/new.html',
        controller: 'DriverController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
