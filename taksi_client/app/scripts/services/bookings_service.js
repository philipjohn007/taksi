'use strict';

var app = angular.module('taksiClientApp');

app.service('BookingsService', function ($resource) {
  console.log($resource);
  return $resource('http://taksi-server-pek.herokuapp.com/bookings', {});
});
