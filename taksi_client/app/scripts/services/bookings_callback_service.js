'use strict';

var app = angular.module('taksiClientApp');

app.service('PusherService', function ($rootScope) {
  var pusher = new Pusher('481bc50052c358efccb6');
  var channel = pusher.subscribe('bookings');
  return {
    onMessage: function (callback) {
      channel.bind('async_notification', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});
