'use strict'

var app = angular.module('taksiClientApp');

app.controller('BookingsController', function ($scope, BookingsService, PusherService) {
  $scope.name = '';
  $scope.address = '';
  $scope.destination = '';
  $scope.email = '';
  $scope.phone = '';
  $scope.syncNotification = '';
  $scope.submit = function() {
    BookingsService.save({name: $scope.name, address: $scope.address, destination: $scope.destination, email: $scope.email, phone: $scope.phone}, function (response) {
      $scope.syncNotification = response.message;
    });
  };

  $scope.asyncNotification = '';
  PusherService.onMessage(function(response) {
    if (!response.message.startsWith('Booking request arrived')) {
      $scope.asyncNotification = response.message;
    }
  });
});
