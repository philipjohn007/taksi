'use strict';

/**
 * @ngdoc function
 * @name taksiClientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the taksiClientApp
 */
angular.module('taksiClientApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
