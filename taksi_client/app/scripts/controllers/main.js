'use strict';

/**
 * @ngdoc function
 * @name taksiClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the taksiClientApp
 */
angular.module('taksiClientApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
