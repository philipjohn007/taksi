'use strict';

describe('BookingsController', function () {

  beforeEach(module('taksiClientApp'));

  var BookingsCtrl,
    scope,
    $httpBackend;

  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_, _BookingsService_, _PusherService_) {
    scope = $rootScope.$new();
    $httpBackend = _$httpBackend_;

    BookingsCtrl = $controller('BookingsController', {
      $scope: scope,
      BookingsService: _BookingsService_,
      PusherService: _PusherService_
    });
  }));

  it('should submit a request to the backend service', function () {
    alert('Hello');

    $httpBackend
      .expectPOST('http://taksi-server-pek.herokuapp.com/bookings',
      {name: '', address: 'Raatuse 22, Tartu, Estonia', destination: 'Liivi 2, Tartu, Estonia', email: '', phone: ''})
      .respond(201, {message: 'Your booking has been registered. Please wait for the confirmation'});

    scope.name = '';
    scope.address = 'Raatuse 22, Tartu, Estonia';
    scope.destination = 'Liivi 2, Tartu, Estonia';
    scope.email = '';
    scope.phone = '';
    scope.submit();
    $httpBackend.flush();

    expect(scope.syncNotification).toBe('Your booking has been registered. Please wait for the confirmation');
  });
});
