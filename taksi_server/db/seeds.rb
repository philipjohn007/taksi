# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
more_drivers = [
    {:name => 'Driver3', :number => '1333', :location => 'Kaubamaja, Tartu, Estonia', :status => 'available', :duration => '311', :totalamt => '0'},
    {:name => 'Driver4', :number => '1444', :location => 'Raatuse 22, Tartu, Estonia', :status => 'available', :duration => '411', :totalamt => '0'},
    {:name => 'Driver5', :number => '1555', :location => 'Raatuse 22, Tartu, Estonia', :status => 'available', :duration => '511', :totalamt => '0'}
]

Driver.send(:attr_accessible, :name, :number, :location, :status)
more_drivers.each do |driver|
  Driver.create!(driver)
end