class CreateDrivers < ActiveRecord::Migration
  def up
    create_table 'drivers' do |t|
      t.string "name"
      t.string "number"
      t.string "location"
      t.string "status"
      t.string "duration"
      t.string "totalamt"
    end
  end

  def down
    drop_table 'drivers'
  end
end
