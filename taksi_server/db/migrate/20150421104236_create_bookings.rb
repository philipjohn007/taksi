class CreateBookings < ActiveRecord::Migration
  def up
    create_table 'bookings' do |t|
      t.string 'name'
      t.string 'address'
      t.string 'destination'
      t.string 'phone'
      t.string 'email'
    end
  end

  def down
    drop_table 'bookings' # deletes the whole table and all its data!
  end
end
