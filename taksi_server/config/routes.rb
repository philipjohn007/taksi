TaksiServer::Application.routes.draw do
  get "/bookings", :to => "bookings#index"
  get "/bookings/address", :to => "bookings#address"
  post "/bookings", :to => "bookings#create"
  match '*all' => "application#cors_preflight", :constraints => {:method => "OPTIONS"}
  get "/drivers", :to => "drivers#index"
  get "/drivers/:id", :to => "drivers#show"
  post "/drivers", :to => "drivers#updateStatus"
  post "/drivers/accept", :to=> "drivers#accept"
  delete "/drivers/accept", :to=> "drivers#reject"
end
