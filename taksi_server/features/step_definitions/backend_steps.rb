address = ''
driver1 = Driver.new()
driver2 = Driver.new()
selecteddriver = Driver.new()
location = ''
latitiude = nil
longitude = nil

Given(/^I am at "(.*?)"$/) do |arg1|
  address = arg1
end

And(/^there are (\d+) taxi available, one next to "(.*?)" and the other at "(.*?)"$/) do |arg1, arg2, arg3|
  driver1.name = 'Driver1'
  driver2.name = 'Driver2'
  driver1.location = arg2
  driver2.location = arg3
  driver1.status = 'available'
  driver2.status = 'available'
  driver1.duration = '110'
  driver2.duration = '220'
end

When(/^I submit a booking request$/) do
  selecteddriver = BookingsHelper.get_closest_driver(address,[driver1,driver2])
end

Then(/^I should receive a confirmation with the taxi next to "(.*?)"$/) do |arg1|
  (selecteddriver.location).should == arg1
end

And(/^I should receive a delay estimate$/) do
end

And(/^there are (\d+) taxi available, one with availability duration "(.*?)" seconds and the other with "(.*?)" seconds$/) do |arg1, arg2, arg3|
  driver1.name = 'Driver1'
  driver2.name = 'Driver2'
  driver1.location = 'Kaubamaja, Tartu, Estonia'
  driver2.location = 'Kaubamaja, Tartu, Estonia'
  driver1.status = 'available'
  driver2.status = 'available'
  driver1.duration = arg2
  driver2.duration = arg3
end

Then(/^I should receive a confirmation with the taxi with "(.*?)" seconds duration$/) do |arg1|
  selecteddriver = BookingsHelper.get_closest_driver(address,[driver1,driver2])
  (selecteddriver.duration).should == arg1
end

Given(/^I am at latitude "(.*?)" and longitude "(.*?)"$/) do |arg1, arg2|
  latitiude = arg1
  longitude = arg2
end

Then(/^my location should be "(.*?)"$/) do |arg1|
  address = Geocoder.get_address(latitiude,longitude)
  (address).should == arg1
end


And(/^the taxi driver accepts the booking$/) do
  selecteddriver = BookingsHelper.get_closest_driver(address,[driver1,driver2])
end

When(/^the taxi driver rejects the booking$/) do
  closest = BookingsHelper.get_closest_driver(address,[driver1,driver2])
  if (closest.name == driver1.name)
    selecteddriver = driver2
  else
    selecteddriver = driver1
  end
end

Given(/^the total amount collected is (\d+)$/) do |arg1|
  driver1.totalamt = arg1
end

When(/^I log in to Taxi Home$/) do
end

Then(/^I should see the amount to be paid as (\d+)$/) do |arg1|
  ((driver1.totalamt.to_i * 3)/100).should == arg1.to_i
end

