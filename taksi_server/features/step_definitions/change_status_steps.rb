driver = Driver.new()
Given(/^I am in "(.*?)" state$/) do |arg1|
  driver.status=arg1
end

And(/^I dropped the customer$/) do

end

When(/^I change the status to "(.*?)"$/) do |arg1|
  driver.status=arg1
end

When(/^I receive a booking request$/) do

end

And(/^I click on Accept$/) do
  driver.status='busy'
end

Then(/^my status should be "(.*?)"$/) do |arg1|
  (driver.status).should == arg1
end