Given(/^I am in the booking page$/) do
 visit "http://localhost:9000/#/bookings"
end

When(/^I provide my details$/) do
  fill_in 'name', with: 'pek'
  fill_in 'address', with: 'Raatuse 22, Tartu, Estonia'
  fill_in 'email', with: 'a@a.a'
  fill_in 'phone', with: '1234'
end

When(/^I submit this information$/) do
  click 'submit-coord'
end

Then(/^I should be notified that my information is being processed$/) do

end

Then(/^I should eventually receive an asynchronous message with my address$/) do

end

