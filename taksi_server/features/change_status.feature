Feature: Change driver status
  As a driver
  So that the system will know my current status
  I want to change my status

  Scenario: Change Busy to Available state
    Given I am in "busy" state
    And I dropped the customer
    When I change the status to "available"
    Then my status should be "available"

  Scenario: Change Available to Busy state
    Given I am in "available" state
    When I receive a booking request
    And I click on Accept
    Then my status should be "busy"

  Scenario: Change Available to Off-Duty state
    Given I am in "available" state
    When I change the status to "off-duty"
    Then my status should be "off-duty"