Feature: Change status and location
  As a driver
  So that the system will know my current status and location
  I want to change my status

  Scenario: Change Busy to Available state in mobile
    Given I am in "busy" state
    And I am at latitude "58.3823671" and longitude "26.7322728"
    When I change the status to "available"
    Then my status should be "available"
    And my location should be "Raatuse 37-39, 50603 Tartu, Estonia"
