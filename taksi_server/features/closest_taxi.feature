Feature: Closest taxi assigning
  As a customer
  So that I can confirm my booking
  I want to one taxi to be booked

  Scenario: Closest taxi
    Given I am at "Raatuse 22, Tartu, Estonia"
    And there are 2 taxi available, one next to "Kaubamaja, Tartu, Estonia" and the other at "Lounakeskus, Tartu, Estonia"
    When I submit a booking request
    Then I should receive a confirmation with the taxi next to "Kaubamaja, Tartu, Estonia"
    And I should receive a delay estimate
