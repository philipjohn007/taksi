Feature: Driver accept request
  As a customer
  So that I can confirm my booking
  I want to one taxi to be booked

  Scenario: The driver of the closest taxi accepts the booking
    Given I am at "Raatuse 22, Tartu, Estonia"
    And there are 2 taxi available, one next to "Raatuse 22, Tartu, Estonia" and the other at "Lounakeskus, Tartu, Estonia"
    When I submit a booking request
    And the taxi driver accepts the booking
    Then I should receive a confirmation with the taxi next to "Raatuse 22, Tartu, Estonia"
    And I should receive a delay estimate