Feature: Show Invoice
  As a driver
  So that I can pay accordingly
  I want to see the invoice

  Scenario: Show Invoice
    Given the total amount collected is 100
    When I log in to Taxi Home
    Then I should see the amount to be paid as 3
