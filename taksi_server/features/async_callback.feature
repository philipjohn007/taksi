#Feature: Geolocalization, asynchronous interactions
#  As a End user
#  So that I can verify the address corresponding to my coordinates
#  I want to get an asynchronous notification with the computed address
#
#  @javascript
#  Scenario: Asynchronous callback
#    Given I am in the booking page
#    When I provide my details
#    And I click on Accept
#    Then I should be notified that my information is being processed
#    And I should eventually receive an asynchronous message with my address