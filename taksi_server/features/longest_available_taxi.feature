Feature: Longest available taxi assigning
  As a customer
  So that I can confirm my booking
  I want to one taxi to be booked

  Scenario: Longest available taxi
    Given I am at "Raatuse 22, Tartu, Estonia"
    And there are 2 taxi available, one with availability duration "110" seconds and the other with "220" seconds
    When I submit a booking request
    Then I should receive a confirmation with the taxi with "220" seconds duration
    And I should receive a delay estimate
