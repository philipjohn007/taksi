require 'spec_helper'

describe Geocoder do
  describe 'get distance' do
    it 'should get the distances between two locations' do
      distance = Geocoder.distance('Kaubamaja, Tartu, Estonia','Kaubamaja, Tartu, Estonia')
      expect (distance).should == 0
    end
  end

  describe 'get duration' do
    it 'should get the duration between two locations' do
      duration = Geocoder.duration('Kaubamaja, Tartu, Estonia','Kaubamaja, Tartu, Estonia')
      expect (duration).should == 1
    end
  end

  describe 'get address' do
    it 'should get the address of the provided latitude and longitude' do
      address = Geocoder.get_address('58.3823671','26.7322728')
      expect (address).should == 'Raatuse 37-39, 50603 Tartu, Estonia'
    end
  end
end