require 'spec_helper'

describe DriversController do
  describe 'get all drivers' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should retrieve driver details' do
      DriversController.stub(:show).and_return(double('Driver'))
      response = get :index
      expect (response.status).should == 200
    end
  end

  describe 'get a driver' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should retrieve driver details' do
      DriversController.stub(:show).and_return(double('Driver'))
      response = get :show, {:id=> 2}
      expect (response.status).should == 200
    end
  end

  describe 'update status' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should update driver status' do
      DriversController.stub(:updateStatus).and_return(double('Driver'))
      response = post :updateStatus, {:driver => {:status => "available"}, :id=> 2}
      expect (response.body).should include("Status updated")
      expect (response.status).should == 201
    end
  end

  describe 'update status and location' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should update driver status and his location when using mobile' do
      DriversController.stub(:updateStatus).and_return(double('Driver'))
      response = post :updateStatus, {:driver => {:status => "available"}, :id=> 2, :latitude => '58.3823671', :longitude => '26.7322728'}
      expect (response.body).should include("Status and location updated")
      expect (response.status).should == 201
    end
  end

  describe 'accept booking' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should accept the booking' do
      DriversController.stub(:accept).and_return(double('Driver'))
      response = post :accept, {"confirm"=>"accepted", "id"=>1, "asyncNotification"=>"Booking request arrived. ID: 102   Name:    Location: Raatuse 22, Tartu, Estonia   Destination: Liivi 2, Tartu, Estonia   Email:    Phone: ", "driver"=>{}}
      expect (response.status).should == 200
    end
  end

  describe 'accept booking and update the amount' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should accept the booking' do
      DriversController.stub(:accept).and_return(double('Driver'))
      response = post :accept, {"confirm"=>"accepted", "id"=>1, "asyncNotification"=>"Booking request arrived. ID: 102   Name:    Location: Raatuse 22, Tartu, Estonia   Destination: Liivi 2, Tartu, Estonia   Email:    Phone: ", "driver"=>{}}
      expect (response.status).should == 200
      @driver = Driver.find(1)
      expect (@driver.totalamt).should start_with('6.9')
    end
  end

  describe 'reject booking' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should reject the booking' do
      DriversController.stub(:reject).and_return(double('Driver'))
      response = delete :reject, {"confirm"=>"accepted", "id"=>3, "asyncNotification"=>"Booking request arrived. ID: 102   Name:    Location: Raatuse 22, Tartu, Estonia   Destination: Liivi 2, Tartu, Estonia   Email:    Phone: ", "driver"=>{}}
      expect (response.status).should == 200
    end
  end
end