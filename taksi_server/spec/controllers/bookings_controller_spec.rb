require 'spec_helper'

describe BookingsController do
  describe 'get all bookings' do
    it 'should create a booking' do
      BookingsController.stub(:index).and_return(double('Booking'))
      response = get :index
      expect (response.status).should == 200
    end
  end

  describe 'create booking' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should create a booking' do
      booking_size = Booking.all.size
      BookingsController.stub(:create).and_return(double('Booking'))
      post :create, {:booking => {:name => "a", :address => 'Raatuse 22, Tartu, Estonia', :destination => 'Liivi 2, Tartu, Estonia', :email => 'a@a.a', :phone => '123'}}
      (Booking.all.size).should == booking_size+1
    end
  end

  describe 'get address' do
    it 'should get the address of the current location' do
      BookingsController.stub(:address).and_return(double('Booking'))
      response = get :address, {:latitude => '58.3823671', :longitude => '26.7322728'}
      expect (response.body).should include("Raatuse 37-39, 50603 Tartu, Estonia")
    end
  end
end