require 'spec_helper'

describe BookingsHelper do
  describe 'get closest driver' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should get the closest driver to an address' do
      drivers = Driver.all
      closest = BookingsHelper.get_closest_driver('Kaubamaja, Tartu, Estonia',drivers)
      expect (closest.location).should == 'Kaubamaja, Tartu, Estonia'
    end
  end

  describe 'longest available driver' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should get the longest available among the closest drivers' do
      drivers = Driver.all
      closest = BookingsHelper.get_closest_driver('Raatuse 22, Tartu, Estonia',drivers)
      expect (closest.name).should == 'Driver5'
    end
  end

  describe 'drivers sorted with distance' do
    before :each do
      load Rails.root + "db/seeds.rb"
    end
    it 'should get the drivers sorted according to the distance from pickup' do
      drivers = Driver.all
      closest_drivers = BookingsHelper.get_closest_drivers('Raatuse 22, Tartu, Estonia',drivers)
      expect (closest_drivers[0].name).should == 'Driver5'
      expect (closest_drivers[1].name).should == 'Driver4'
      expect (closest_drivers[2].name).should == 'Driver3'
    end
  end
end