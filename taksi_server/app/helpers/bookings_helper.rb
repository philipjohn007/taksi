module BookingsHelper
  def self.get_closest_driver(address, drivers)
    available_drivers = drivers.select { |driver| driver.status == 'available' }
    closest_driver = nil
    distances = Array.new()
    nearest_distance = Geocoder.distance(address,available_drivers[0].location)
    available_drivers.each do |driver|
      current_distance = Geocoder.distance(address,driver.location)
      distances.push(current_distance)
      if Geocoder.distance(address,driver.location)<nearest_distance
        nearest_distance = current_distance
      end
    end

    i = 0
    duration = ''
    distances.each do |distance|
      if (distance == nearest_distance) && (duration<available_drivers[i].duration)
        duration = available_drivers[i].duration
        closest_driver = available_drivers[i]
      end
      i+=1
    end
    return closest_driver
  end

  def self.get_closest_drivers(address, drivers)
    closest_drivers = Array.new()
    available_drivers = drivers.select { |driver| driver.status == 'available' }
    count = available_drivers.length
    begin
      closest_driver = nil
      distances = Array.new()
      nearest_distance = Geocoder.distance(address,available_drivers[0].location)
      available_drivers.each do |driver|
        current_distance = Geocoder.distance(address,driver.location)
        distances.push(current_distance)
        if Geocoder.distance(address,driver.location)<nearest_distance
          nearest_distance = current_distance
        end
      end

      i = 0
      duration = ''
      driver_count = 0
      distances.each do |distance|
        if (distance == nearest_distance) && (duration<available_drivers[i].duration)
          duration = available_drivers[i].duration
          closest_driver = available_drivers[i]
          driver_count = i
        end
        i+=1
      end
      closest_drivers.push(closest_driver)
      available_drivers.delete_at(driver_count)
    end until closest_drivers.length==count

    return closest_drivers
  end

end