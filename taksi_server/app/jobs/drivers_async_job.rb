class DriversAsyncJob
  include SuckerPunch::Job
  def perform(message,id)
    Pusher['drivers'+id.to_s].trigger('async_notification'+id.to_s, {:message => message, :id => id})
  end
end