class BookingsAsyncJob
  include SuckerPunch::Job
  def perform(message)
    Pusher['bookings'].trigger('async_notification', {:message => message})
  end
end