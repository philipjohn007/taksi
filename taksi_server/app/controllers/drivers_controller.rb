class DriversController < ApplicationController
  def index
    @drivers = Driver.all()
    render :text => @drivers.to_json, :status => :ok
  end

  def show
    @driver = Driver.find(params[:id])
    render :text => @driver.to_json, :status => :ok
  end

  def updateStatus
    @driver = Driver.find(params[:id])
      if (params[:latitude] != nil && params[:longitude] != nil)
        address = Geocoder.get_address(params[:latitude],params[:longitude])
        @driver.update_attribute(:location,address)
        @driver.update_attribute(:status,params[:status])
        render :text => {:message => "Status and location updated"}.to_json, :status => :created
      else
        @driver.update_attributes!(params[:driver])
        render :text => {:message => "Status updated"}.to_json, :status => :created
      end
  end

  def accept
    message = params[:asyncNotification]
    address = message.split("Location: ")[1].split("   Destination")[0]
    destination = message.split("Destination: ")[1].split("   Email")[0]
    @driver = Driver.find(params[:id])
    distance = Geocoder.distance(address,destination)
    journey_cost = (distance.to_f/1000)*3
    amount = @driver.totalamt.to_f + journey_cost
    @driver.update_attribute(:totalamt,amount.to_s)
    @driver.update_attribute(:status,'busy')
    duration = Geocoder.duration(address,@driver.location)
    render :text => {:message => "Customer Assigned. Cost: "+journey_cost.to_s}.to_json, :status => :ok
    BookingsAsyncJob.new.async.perform("Driver Assigned: "+ @driver.name + " :: Taxi will arrive in "+duration.to_s+" mins :: Estimated Cost: "+journey_cost.round(2).to_s+" euros")
  end

  def reject
    render :text => {:message => "Booking Rejected"}.to_json, :status => :ok
    @driver = Driver.find(params[:id])
    @drivers = Driver.all()
    address = params[:asyncNotification].split("Location: ")[1].split("   Destination")[0]
    @closest_drivers = BookingsHelper.get_closest_drivers(address,@drivers)
    @next_driver = nil
    i=0
    @closest_drivers.each do |drvr|
      if (drvr.id.to_s==params[:id])
        if (@closest_drivers[i+1] != nil)
          @next_driver = @closest_drivers[i+1]
        end
      end
      i+=1
    end
    @drivermessage = params[:asyncNotification]
    if @next_driver != nil
      DriversAsyncJob.new.async.perform(@drivermessage,@next_driver.id)
    end
  end
end