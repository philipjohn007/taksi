class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :cors_preflight

  def cors_preflight
    headers["Access-Control-Allow-Origin"] = "*"
    headers["Access-Control-Allow-Methods"] = 'GET,POST,PUT,DELETE,OPTIONS'
    headers["Access-Control-Allow-Headers"] = 'Origin,Accept,Content-Type'
    headers['Access-Control-Max-Age'] = "3628800"
    head(:ok) if request.request_method == "OPTIONS"
  end
end
