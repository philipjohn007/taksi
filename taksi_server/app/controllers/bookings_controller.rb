class BookingsController < ApplicationController
  def index
    @bookings = Booking.all()
    render :text => @bookings.to_json, :status => :ok
  end

  def create
    @booking = Booking.create!(params[:booking])
    @drivers = Driver.all()
    @closest_driver = BookingsHelper.get_closest_driver(@booking.address,@drivers)
    render :text => {:message => "Your booking has been registered. Please wait for the confirmation"}.to_json, :status => :created
    @drivermessage = "Booking request arrived. ID: "+@booking.id.to_s+"   Name: "+ @booking.name+"   Location: "+@booking.address+"   Destination: "+@booking.destination+"   Email: "+@booking.email+"   Phone: "+@booking.phone
    DriversAsyncJob.new.async.perform(@drivermessage,@closest_driver.id)
  end

  def address
    latitude = params[:latitude]
    longitude = params[:longitude]
    address = Geocoder.get_address(latitude,longitude)
    render :text => {:message => address}.to_json, :status => :ok
  end
end