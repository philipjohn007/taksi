class Booking < ActiveRecord::Base
  attr_accessible :name, :address, :destination, :phone, :email
end