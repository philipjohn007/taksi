class Geocoder
  require 'uri'
  require 'net/http'
  require 'json'

  def self.get_address(latitude, longitude)
    latitude_enc = URI.escape(latitude)
    longitude_enc = URI.escape(longitude)
    uri = URI('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude_enc + ',' + longitude_enc)
    result = Net::HTTP.get(uri)
    json=JSON.parse(result)
    return  (json["results"][0]["formatted_address"])
  end

  def self.distance(address1,address2)
    json = nil
    loop do
      address1_enc = URI.escape(address1)
      address2_enc = URI.escape(address2)
      uri = URI('http://maps.googleapis.com/maps/api/directions/json?origin=' + address1_enc + '&destination=' + address2_enc + '&sensor=false')
      result = Net::HTTP.get(uri)
      json=JSON.parse(result)
      break if (json != nil && json["routes"] != nil && json["routes"][0] != nil && json["routes"][0]["legs"][0] != nil)
    end
    return  (json["routes"][0]["legs"][0]["distance"]["value"])
  end

  def self.duration(address1,address2)
    json = nil
    loop do
      address1_enc = URI.escape(address1)
      address2_enc = URI.escape(address2)
      uri = URI('http://maps.googleapis.com/maps/api/directions/json?origin=' + address1_enc + '&destination=' + address2_enc + '&sensor=false')
      result = Net::HTTP.get(uri)
      json=JSON.parse(result)
      break if (json != nil && json["routes"] != nil && json["routes"][0] != nil && json["routes"][0]["legs"][0] != nil)
    end
    seconds = (json["routes"][0]["legs"][0]["duration"]["value"])
    seconds = seconds + 0.2*seconds + 60
    return Integer(seconds/60)
  end
end